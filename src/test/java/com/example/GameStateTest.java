package com.example;

import static com.example.GameState.NONE;
import static com.example.GameState.RED0;
import static com.example.GameState.YELL;
import static org.junit.Assert.*;

/**
 * Created by romain on 21/02/17.
 */
public class GameStateTest {
    @org.junit.Test
    public void check01() throws Exception {

        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, RED0, RED0, NONE, NONE, NONE},
                {NONE, NONE, YELL, RED0, NONE, NONE, NONE},
                {YELL, RED0, RED0, YELL, RED0, NONE, YELL},
                {RED0, YELL, YELL, YELL, RED0, RED0, YELL}

        };

        GameState gameState = new GameState(gameStateMatrix);
        assertEquals(RED0, gameState.check());
    }

    @org.junit.Test
    public void check02() throws Exception {

        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, RED0, NONE, NONE, NONE},
                {NONE, NONE, YELL, RED0, NONE, NONE, NONE},
                {YELL, RED0, RED0, YELL, RED0, NONE, YELL},
                {RED0, YELL, YELL, YELL, RED0, RED0, YELL}

        };

        GameState gameState = new GameState(gameStateMatrix);
        assertEquals(NONE, gameState.check());
    }


    @org.junit.Test
    public void check03() throws Exception {

        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, RED0, RED0, RED0, RED0, NONE},
                {NONE, NONE, YELL, RED0, NONE, NONE, NONE},
                {YELL, RED0, RED0, YELL, RED0, NONE, YELL},
                {RED0, YELL, YELL, YELL, RED0, RED0, YELL}

        };

        GameState gameState = new GameState(gameStateMatrix);
        assertEquals(RED0, gameState.check());
    }


    @org.junit.Test
    public void check04() throws Exception {

        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, RED0, YELL, RED0, RED0, NONE},
                {NONE, NONE, YELL, YELL, NONE, NONE, NONE},
                {YELL, RED0, RED0, YELL, RED0, NONE, YELL},
                {RED0, YELL, YELL, YELL, RED0, RED0, YELL}

        };

        GameState gameState = new GameState(gameStateMatrix);
        assertEquals(YELL, gameState.check());
    }

    @org.junit.Test
    public void checkv1() throws Exception {

        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {YELL, NONE, NONE, NONE, NONE, NONE, NONE},
                {YELL, NONE, NONE, NONE, NONE, NONE, NONE},
                {YELL, NONE, NONE, NONE, NONE, NONE, NONE},
                {YELL, NONE, NONE, NONE, NONE, NONE, NONE}

        };

        GameState gameState = new GameState(gameStateMatrix);
        assertEquals(YELL, gameState.check());
    }

    @org.junit.Test
    public void checkv2() throws Exception {

        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, YELL, NONE, NONE, NONE, NONE, NONE},
                {NONE, YELL, NONE, NONE, NONE, NONE, NONE},
                {NONE, YELL, NONE, NONE, NONE, NONE, NONE},
                {NONE, YELL, NONE, NONE, NONE, NONE, NONE}

        };

        GameState gameState = new GameState(gameStateMatrix);
        assertEquals(YELL, gameState.check());
    }


    @org.junit.Test
    public void checkv3() throws Exception {

        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, YELL, NONE, NONE, NONE, NONE},
                {NONE, NONE, YELL, NONE, NONE, NONE, NONE},
                {NONE, NONE, YELL, NONE, NONE, NONE, NONE},
                {NONE, NONE, YELL, NONE, NONE, NONE, NONE}

        };

        GameState gameState = new GameState(gameStateMatrix);
        assertEquals(YELL, gameState.check());
    }

    @org.junit.Test
    public void checkv4() throws Exception {

        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, YELL, NONE, NONE, NONE},
                {NONE, NONE, NONE, YELL, NONE, NONE, NONE},
                {NONE, NONE, NONE, YELL, NONE, NONE, NONE},
                {NONE, NONE, NONE, YELL, NONE, NONE, NONE}

        };

        GameState gameState = new GameState(gameStateMatrix);
        assertEquals(YELL, gameState.check());
    }

    @org.junit.Test
    public void checkh1() throws Exception {

        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, YELL, YELL, YELL, YELL},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE}

        };

        GameState gameState = new GameState(gameStateMatrix);
        assertEquals(YELL, gameState.check());
    }


    @org.junit.Test
    public void checkh2() throws Exception {

        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, YELL, YELL, YELL, YELL},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE}

        };

        GameState gameState = new GameState(gameStateMatrix);
        assertEquals(YELL, gameState.check());
    }


    @org.junit.Test
    public void checkh3() throws Exception {

        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, YELL, YELL, YELL, YELL},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE}

        };

        GameState gameState = new GameState(gameStateMatrix);
        assertEquals(YELL, gameState.check());
    }


    @org.junit.Test
    public void checkh4() throws Exception {

        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, YELL, YELL, YELL, YELL}

        };

        GameState gameState = new GameState(gameStateMatrix);
        assertEquals(YELL, gameState.check());
    }
}