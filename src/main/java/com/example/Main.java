package com.example;

import static com.example.GameState.*;

public class Main
{

    public static void main(String [] args)
    {
        Integer[][] gameStateMatrix = new Integer[][] {
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, NONE, NONE, NONE, NONE, NONE},
                {NONE, NONE, RED0, RED0, NONE, NONE, NONE},
                {NONE, NONE, YELL, RED0, NONE, NONE, NONE},
                {YELL, RED0, RED0, YELL, RED0, NONE, YELL},
                {RED0, YELL, YELL, YELL, RED0, RED0, YELL}

        };

        GameState gameState = new GameState(gameStateMatrix);

        switch (gameState.check()) {
            case NONE:
                System.out.println("Nobody wins");
                break;
            case RED0:
                System.out.println("Red wins");
                break;
            case YELL:
                System.out.println("Yellow wins");

        }

    }

}