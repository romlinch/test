package com.example;

import java.util.Vector;

/**
 * Created by romain on 20/02/17.
 */

public class GameState {

    public static final int NONE=0;
    public static final int RED0=1;
    public static final int YELL=2;

    private static int WIN_LENGTH = 4;

    private final Integer[][]  matrix;

    public GameState(Integer[][]  matrix) {
        this.matrix = matrix;
    }

    /**
     * Check if there is a winner in current game state
     * @return an integer representing the winner
     */
    public int check() {

        // Check in each square of WIN_LENGTH x WIN_LENGTH
        final int ymax = matrix.length - (WIN_LENGTH -1);
        final int xmax = matrix[0].length - (WIN_LENGTH -1);
        for(int y=0; y<ymax; y++) {
            for(int x=0; x<xmax; x++) {
                int result = check(x, y);
                if( result> 0) {
                    return result;
                }

            }
        }
        return NONE;
    }

    /**
     * Check sub matrix 4x4
     * @param x starting abscisse
     * @param y starting ordinal
     * @return
     */
    private int check(int x, int y) {
        int result = NONE;

        result = checkDiags(x,y);
        if(result > 0 ) {
            return result;
        }


        result = checkVertical(x,y);
        if(result > 0 ) {
            return result;
        }
        result = checkVertical(x+1,y);
        if(result > 0 ) {
            return result;
        }
        result = checkVertical(x+2,y);
        if(result > 0 ) {
            return result;
        }
        result = checkVertical(x+3,y);
        if(result > 0 ) {
            return result;
        }

        result = checkHorizontal(x, y);
        if(result > 0 ) {
            return result;
        }
        result = checkHorizontal(x, y+1);
        if(result > 0 ) {
            return result;
        }
        result = checkHorizontal(x, y+2);
        if(result > 0 ) {
            return result;
        }
        result = checkHorizontal(x, y+3);
        if(result > 0 ) {
            return result;
        }
        return result;
    }

    private int checkVertical(int x, int y) {
        if( matrix[y][x] == matrix[y+1][x] &&
                matrix[y][x] == matrix[y+2][x] &&
                matrix[y][x] == matrix[y+3][x]) {
            return  matrix[y][x];
        } else {
            return NONE;
        }
    }

    private int checkHorizontal(int x, int y) {
        if( matrix[y][x] == matrix[y][x+1] &&
                matrix[y][x] == matrix[y][x+2] &&
                matrix[y][x] == matrix[y][x+3]) {
            return  matrix[y][x];
        } else {
            return NONE;
        }
    }

    private int checkDiags(final int y,final  int x) {
        if(     matrix[x][y] == matrix[x+1][y+1]
                && matrix[x][y] == matrix[x+2][y+2]
                && matrix[x][y] == matrix[x+3][y+3]

                ||

                matrix[x+3][y] == matrix[x+2][y+1]
                        && matrix[x+3][y] == matrix[x+1][y+2]
                        && matrix[x+3][y] == matrix[x][y+3]
                ) {
            return matrix[x][y];
        } else {
            return NONE;
        }

    }

}
